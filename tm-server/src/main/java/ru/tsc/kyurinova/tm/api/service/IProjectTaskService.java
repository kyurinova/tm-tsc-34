package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @Nullable
    Task bindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    Task unbindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeAllTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Project removeById(@Nullable String userId, @Nullable String projectId);

    @NotNull
    List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId);

}
